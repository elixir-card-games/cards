defmodule Cards.Deck do
  @moduledoc false

  @type t :: list(Cards.t())

  @doc """
  Shuffles a deck of cards

  Returns a *new* shuffled deck of cards
  """
  @spec shuffle(t) :: t
  def shuffle(deck) do
    Enum.shuffle(deck)
  end

  @doc """
  Peeks at the top cards in a deck

  This does *not* remove the cards from the deck
  """
  @spec peek_top(t, integer) :: {:ok, t} | {:error, atom}
  def peek_top(deck, amount \\ 1) do
    if length(deck) > 0 do
      {:ok, deck |> Enum.take(amount)}
    else
      {:error, :empty_deck}
    end
  end

  @doc """
  Peeks at the top cards in a deck

  This does *not* remove the cards from the deck
  """
  @spec peek_top!(t, integer) :: t | String.t()
  def peek_top!(deck, amount \\ 1) do
    case peek_top(deck, amount) do
      {:ok, cards} -> cards
      {:error, :empty_deck} -> raise "Can't peek into an empty deck"
    end
  end

  def draw_card(deck, location \\ :top)

  def draw_card(deck, location) when location === :top do
    if length(deck) > 0 do
      {drawn, remaining} = List.pop_at(deck, 0)
      {:ok, drawn, remaining}
    else
      {:error, :empty_deck}
    end
  end

  def draw_card(deck, location) when location === :bottom do
    if length(deck) > 0 do
      {drawn, remaining} = List.pop_at(deck, -1)
      {:ok, drawn, remaining}
    else
      {:error, :empty_deck}
    end
  end

  def draw_card(deck, location) when location === :random do
    if length(deck) > 0 do
      {drawn, remaining} = List.pop_at(deck, :rand.uniform(length(deck)) - 1)
      {:ok, drawn, remaining}
    else
      {:error, :empty_deck}
    end
  end

  def take_card(deck, card) do
    if length(deck) > 0 do
      new_deck = deck |> Enum.reject(&(&1 === card))

      if length(new_deck) === length(deck) do
        {:error, :card_not_found}
      else
        {:ok, new_deck}
      end
    else
      {:error, :empty_deck}
    end
  end

  def put_card(deck, card, location \\ :top)

  def put_card(deck, card, location) when location === :top do
    [card | deck]
  end

  def put_card(deck, card, location) when location === :bottom do
    deck ++ [card]
  end

  def put_card(deck, card, location) when location === :random do
    List.insert_at(deck, :rand.uniform(length(deck)) - 1, card)
  end

  def split(deck, number \\ 2) do
    if length(deck) > 0 do
      new_deck_card_count = length(deck) |> div(number)
      {split_deck, leftover_deck} = Enum.split(deck, new_deck_card_count * number)
      split_decks = Enum.chunk_every(split_deck, new_deck_card_count)
      {:ok, split_decks, leftover_deck}
    else
      {:error, :empty_deck}
    end
  end
end
