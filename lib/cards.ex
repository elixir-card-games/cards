defmodule Cards do
  @moduledoc """
  Card manipulation library

  This library contains basic functionality related to using and manipulating
  of cards.

  ## Terminology
  A **Set** of cards is a user-defined collection of cards that function
  together. For instance, the classic 52 playing cards consisting of 4 suits
  can be defined as a **set**

  A **Deck** of cards is any list of cards, from the _same_ **Set**. The same
  **set** requirement allows for **deck** operations that require comparisons
  between the cards. Cards in a specific **set** may not necessarily be
  comparable to cards in a different one.

  A **Card** is simply a user-defined structure that represents a single
  card in a **set**.

  ## Usage
  To use this library, you need to define a set of cards that implements the
  behaviour `Cards.Set`. Once this has been done you can draw a deck from a
  given set of cards and use the functions defined in this module to manipulate
  the given **deck**.

  """

  alias Cards.Deck

  @typedoc """
  A `t:struct/0` representing a single card in a set
  """
  @type t :: struct()

  @typedoc """
  A `t:list/1` of `t:card/0` types, representing a deck of cards
  """
  @type deck :: list(t())

  @doc """
  Shuffle the given `deck` of cards

  ## Returns
  - `t:deck/0` - New, shuffled deck of cards
  """
  @spec shuffle(deck) :: deck
  def shuffle(deck) do
    deck |> Deck.shuffle()
  end

  @doc """
  Peek at the top cards in a deck

  This does not remove the cards from the deck

  ## Options
  - `amount`: `t:integer/0` - number of cards to peek at. Defaults to `1`

  ## Returns
  - `{:ok, deck}` - a `t:deck/0` of peeked cards
  - `{:error, :empty_deck}` - if the deck is empty
  """
  @spec peek_top(deck, keyword()) :: {:ok, deck} | {:error, atom()}
  def peek_top(deck, opts \\ []) do
    deck |> Deck.peek_top(opts |> Keyword.get(:amount, 1))
  end

  @doc """
  Peek at the top cards in a deck

  See `Cards.peek_top/2`
  """
  @spec peek_top!(deck, keyword()) :: deck
  def peek_top!(deck, opts \\ []) do
    deck |> Deck.peek_top!(opts |> Keyword.get(:amount, 1))
  end

  @doc """
  Draw a single card from a deck

  ## Options
  - `location`: `t:atom/0` - where to draw the card from. The options are `:top`,
    `:bottom` and `:random`. Defaults to `:top`.

  ## Returns
  - `{:ok, drawn, remaining}` - the drawn `t:card/0` as well as the remaining
    `t:deck/0` of cards.
  - `{:error, :empty_deck}` - if the deck is empty
  """
  @spec draw_card(deck, keyword) :: {:ok, t, deck} | {:error, atom()}
  def draw_card(deck, opts \\ []) do
    deck |> Deck.draw_card(opts |> Keyword.get(:location, :top))
  end

  @doc """
  Take the given card from a deck

  ## Returns
  - `{:ok, deck}` - the new deck of cards, minus the taken card
  - `{:error, :card_not_found}` - if the card isn't found in the deck
  - `{:error, :empty_deck}` - if the deck is empty
  """
  @spec take_card(deck, t) :: {:ok, deck} | {:error, atom}
  def take_card(deck, card) do
    deck |> Deck.take_card(card)
  end

  @doc """
  Place a card in a deck

  ## Options
  - `location`: `t:atom/0` - where to place the card. The options are `:top`,
    `:bottom` and `:random`. Defaults to `:top`.

  ## Returns
  - `t:deck.t/0` - the new deck of cards
  """
  @spec put_card(deck, t, keyword) :: deck
  def put_card(deck, card, opts \\ []) do
    deck |> Deck.put_card(card, opts |> Keyword.get(:location, :top))
  end

  @doc """
  Split the given deck of cards

  ## Options
  - `number`: `t:integer/0` - number of resulting decks after the split.
    Defaults to `2`

  ## Returns
  - `{:ok, split_decks, leftover_deck}` - a list of decks from the split, as
    well as a deck of leftover cards after uneven splits.
  - `{:error, :empty_deck}` - if the deck is empty
  """
  @spec split_deck(deck, keyword) :: {:ok, list(deck), deck} | {:error, atom}
  def split_deck(deck, opts \\ []) do
    deck |> Deck.split(opts |> Keyword.get(:number, 2))
  end
end
