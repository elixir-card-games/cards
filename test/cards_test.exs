defmodule CardsTest do
  use ExUnit.Case
  alias Cards.Sets.Classic
  doctest Cards

  setup do
    {:ok, deck: Classic.init()}
  end

  describe "basic api:" do
    @tag :api
    test "shuffles a deck of cards", state do
      shuffled_deck = Cards.shuffle(state[:deck])

      refute Enum.zip([shuffled_deck, state[:deck]])
             |> Enum.reduce(true, fn {a, b}, acc -> acc && a === b end)
    end

    @tag :api
    test "peeks at the top of a deck of cards", state do
      {:ok, cards} = Cards.peek_top(state[:deck])
      assert length(cards) === 1
      assert hd(cards) === hd(state[:deck])

      cards = Cards.peek_top!(state[:deck])
      assert hd(cards) === hd(state[:deck])

      {:ok, cards} = Cards.peek_top(state[:deck], amount: 3)
      assert length(cards) === 3
    end

    @tag :api
    test "draws a card from a deck", state do
      top_card = state[:deck] |> Enum.at(0)
      {:ok, drawn, remaining} = state[:deck] |> Cards.draw_card()
      assert drawn === top_card
      assert length(remaining) === length(state[:deck]) - 1

      bottom_card = state[:deck] |> List.last()
      {:ok, drawn, remaining} = state[:deck] |> Cards.draw_card(location: :bottom)
      assert drawn === bottom_card
      assert length(remaining) === length(state[:deck]) - 1
    end

    @tag :api
    test "takes a specific card from a deck", state do
      card = hd(state[:deck])
      {:ok, deck} = state[:deck] |> Cards.take_card(card)

      assert hd(deck) === Enum.at(state[:deck], 1)
      assert length(deck) + 1 === length(state[:deck])
    end

    @tag :api
    test "puts a card into or onto a deck", state do
      card = state[:deck] |> Enum.at(1)
      new_deck = state[:deck] |> Cards.put_card(card)

      assert hd(new_deck) === card
      assert length(new_deck) - 1 === length(state[:deck])

      new_deck = state[:deck] |> Cards.put_card(card, location: :bottom)
      assert Enum.at(new_deck, -1) === card
      assert length(new_deck) - 1 === length(state[:deck])

      new_deck = state[:deck] |> Cards.put_card(card, location: :random)
      assert length(new_deck) - 1 === length(state[:deck])
    end

    @tag :api
    test "splits a deck into multiple decks", state do
      # Standard deck has 52 cards
      {:ok, decks, leftover} = Cards.split_deck(state[:deck], number: 3)

      # Expect split into 3 as requested
      assert length(decks) === 3
      # Expect 17 cards per deck, and 1 leftover since 52 / 3 = 17 rem 1
      assert length(decks |> hd) === 17
      assert length(leftover) === 1
    end
  end
end
